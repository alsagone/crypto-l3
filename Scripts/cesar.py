def borner(val, minimum, maximum):
    diff = maximum - minimum
    return ((val - minimum) % diff) + minimum


def chiffrer(char: str, decalage: int) -> str:
    # Si le char n'est pas une lettre, retourner le char tel quel
    if (not(char.isalpha())):
        return char

    # On prend le code ascii du char et on y ajoute decalage
    # Ensuite, on utilise la fonction borner dans les cas où n est hors limites

    n = ord(char) + decalage

    # Si la lettre est en majuscule
    if (char == char.upper()):
        nouvel_ascii = borner(n, ord('A'), ord('Z'))

    else:
        nouvel_ascii = borner(n, ord('a'), ord('z'))

    return chr(nouvel_ascii)
